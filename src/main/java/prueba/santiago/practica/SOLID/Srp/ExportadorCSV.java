package prueba.santiago.practica.SOLID.Srp;

import java.util.List;
import java.util.stream.Collectors;

public class ExportadorCSV {

    public String exportar (List<Pelicula> peliculas){
        return peliculas.stream().map(pelicula ->(pelicula.getTitulo()+", "+pelicula.getGenero()+", "+pelicula.getDirector()))
                .collect(Collectors.joining("\n"));
    }

}
